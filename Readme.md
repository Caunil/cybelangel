## Data scientist - Technical check-up - SMS - CybelAngel

Posted by Lucain Pouget.

### Deliverables

* Data scientist - Technical check-up - SMS.pdf
* script.sh -> One line command that copy all text files from a folder and paste them in another folder with a prefix "new_".
* Pipfile / Pipfile.lock : requirements needed to run the project. This project run on Python3.6 .
* Programming questions.ipynb -> Python Notebook with 2 programming questions about palindromes and anagrams
* spam.csv -> data for the Kaggle Challenge
* Spam Classifier.ipynb -> Python Notebook implementing the Spam Classifier
* Data scientist - CybelAngel - Lucain Pouget.pdf -> Slidedeck presenting the project

### Getting started

#### Script question

Run the following command in terminal
```
sh script.sh

# or

ls folder1 | grep \.txt$ | xargs -I xx cp folder1/xx folder2/new_xx
```

#### Python questions

Initialize your Python environment. You should have Python 3.6 installed.
```
pipenv install
pipenv shell
```
Now a virtualenv is set with all packages installed.

Then, run
```
jupyter notebook
```

And open `Programming questions.ipynb` and `Spam Classifier.ipynb`.
